# travel-click-docker

Provides a common base docker image for travel.click projects. This image comes with many npm packages pre-installed. The goal is to reduce image size and build time.

## Usage

Copy `travel-click-update` into your system path and make it executable. Run `travel-click-update` instead of `npm update` for your travel.click projects.

Use `echo $PATH` to find your system path.
